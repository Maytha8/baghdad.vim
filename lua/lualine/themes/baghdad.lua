-- Lualine theme file
--         ____              _         _           _ 
--        | __ )  __ _  __ _| |__   __| | __ _  __| |
--        |  _ \ / _` |/ _` | '_ \ / _` |/ _` |/ _` |
--        | |_) | (_| | (_| | | | | (_| | (_| | (_| |
--        |____/ \__,_|\__, |_| |_|\__,_|\__,_|\__,_|
--                     |___/                         
--
--          "A wonderful dark color scheme for Vim"
--
-- File:         lua/lualine/themes/baghdad.lua
-- URL:          github.com/baghdad-theme/vim
-- Maintainer:   Maytha8 <maytha8thedev@gmail.com>
-- License:      MIT
--
-- Copyright (c) 2023 Maytha8
--
-- Permission is hereby granted, free of charge, to any per‐
-- son obtaining a copy of this software and associated doc‐
-- umentation  files  (the “Software”), to deal in the Soft‐
-- ware without restriction,  including  without  limitation
-- the rights to use, copy, modify, merge, publish, distrib‐
-- ute, sublicense, and/or sell copies of the Software,  and
-- to permit persons to whom the Software is furnished to do
-- so, subject to the following conditions:
--
-- The above copyright notice  and  this  permission  notice
-- shall  be  included in all copies or substantial portions
-- of the Software.
--
-- THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY
-- KIND,  EXPRESS  OR  IMPLIED, INCLUDING BUT NOT LIMITED TO
-- THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICU‐
-- LAR  PURPOSE  AND  NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE  LIABLE  FOR  ANY  CLAIM,
-- DAMAGES  OR OTHER LIABILITY, WHETHER IN AN ACTION OF CON‐
-- TRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CON‐
-- NECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.

local colors = {
  black        = '#282828',
  white        = '#ebdbb2',
  red          = '#783128',
  green        = '#93ba57',
  blue         = '#77d6a6',
  yellow       = '#f0da42',
  paleyellow   = '#f0ce7d',
  gray         = '#4f4f4f',
  darkgray     = '#151515',
  lightgray    = '#212121',
  inactivegray = '#1a1a1a',
}
return {
  normal = {
    a = { bg = colors.paleyellow, fg = colors.black, gui = 'bold' },
    b = { bg = colors.lightgray, fg = colors.white },
    c = { bg = colors.darkgray, fg = colors.gray }
  },
  insert = {
    a = { bg = colors.blue, fg = colors.black, gui = 'bold' },
    b = { bg = colors.lightgray, fg = colors.white },
    c = { bg = colors.lightgray, fg = colors.white }
  },
  visual = {
    a = { bg = colors.green, fg = colors.black, gui = 'bold' },
    b = { bg = colors.lightgray, fg = colors.white },
    c = { bg = colors.inactivegray, fg = colors.gray }
  },
  replace = {
    a = { bg = colors.red, fg = colors.white, gui = 'bold' },
    b = { bg = colors.lightgray, fg = colors.white },
    c = { bg = colors.black, fg = colors.white }
  },
  command = {
    a = { bg = colors.yellow, fg = colors.black, gui = 'bold' },
    b = { bg = colors.lightgray, fg = colors.white },
    c = { bg = colors.inactivegray, fg = colors.gray }
  },
  inactive = {
    a = { bg = colors.darkgray, fg = colors.gray, gui = 'bold' },
    b = { bg = colors.darkgray, fg = colors.gray },
    c = { bg = colors.darkgray, fg = colors.gray }
  }
}
