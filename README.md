<div align="center">
    <h1>baghdad.vim</h1>
    <strong>A wonderful dark color scheme for Vim</strong>
</div>

Also includes a [lualine](https://github.com/nvim-lualine/lualine.nvim) theme.

To install, use your favourite plugin manager to add `baghdad-theme/baghdad.vim`,
then set the color scheme using `:colorscheme`.

[lazy.nvim](https://github.com/folke/lazy.nvim) example:
```
require('lazy').setup({
  {
    'baghdad-theme/baghdad.vim',
    config = function () vim.cmd.colorscheme('baghdad') end,
  },
})
```

## License

MIT. License can be found below, as well as in [LICENSE](./LICENSE).

Copyright &copy; 2023 Maytha8

Permission is hereby granted, free of charge, to any per‐
son obtaining a copy of this software and associated doc‐
umentation  files  (the “Software”), to deal in the Soft‐
ware without restriction,  including  without  limitation
the rights to use, copy, modify, merge, publish, distrib‐
ute, sublicense, and/or sell copies of the Software,  and
to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice  and  this  permission  notice
shall  be  included in all copies or substantial portions
of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY
KIND,  EXPRESS  OR  IMPLIED, INCLUDING BUT NOT LIMITED TO
THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICU‐
LAR  PURPOSE  AND  NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE  LIABLE  FOR  ANY  CLAIM,
DAMAGES  OR OTHER LIABILITY, WHETHER IN AN ACTION OF CON‐
TRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CON‐
NECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
