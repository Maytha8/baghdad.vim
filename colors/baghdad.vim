" Vim color file
"         ____              _         _           _ 
"        | __ )  __ _  __ _| |__   __| | __ _  __| |
"        |  _ \ / _` |/ _` | '_ \ / _` |/ _` |/ _` |
"        | |_) | (_| | (_| | | | | (_| | (_| | (_| |
"        |____/ \__,_|\__, |_| |_|\__,_|\__,_|\__,_|
"                     |___/                         
"
"          "A wonderful dark color scheme for Vim"
"
" File:         baghdad.vim
" URL:          github.com/baghdad-theme/baghdad.vim
" Maintainer:   Maytha8 <maytha8thedev@gmail.com>
" License:      MIT
"
" Copyright (c) 2023 Maytha8
"
" Permission is hereby granted, free of charge, to any per‐
" son obtaining a copy of this software and associated doc‐
" umentation  files  (the “Software”), to deal in the Soft‐
" ware without restriction,  including  without  limitation
" the rights to use, copy, modify, merge, publish, distrib‐
" ute, sublicense, and/or sell copies of the Software,  and
" to permit persons to whom the Software is furnished to do
" so, subject to the following conditions:
"
" The above copyright notice  and  this  permission  notice
" shall  be  included in all copies or substantial portions
" of the Software.
"
" THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY
" KIND,  EXPRESS  OR  IMPLIED, INCLUDING BUT NOT LIMITED TO
" THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICU‐
" LAR  PURPOSE  AND  NONINFRINGEMENT. IN NO EVENT SHALL THE
" AUTHORS OR COPYRIGHT HOLDERS BE  LIABLE  FOR  ANY  CLAIM,
" DAMAGES  OR OTHER LIABILITY, WHETHER IN AN ACTION OF CON‐
" TRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CON‐
" NECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
" THE SOFTWARE.

set background=dark

hi clear

if exists("syntax_on")
  syntax reset
endif

let colors_name = "baghdad"

hi Normal           guifg=#ffffff guibg=#121212 gui=NONE                                  cterm=NONE
hi StatusLine       guifg=#c9c9c9 guibg=#262626 gui=NONE                                  cterm=NONE
hi StatusLineNC     guifg=#666666 guibg=#151515 gui=NONE                                  cterm=NONE
hi StatusLineTerm   guifg=#c9c9c9 guibg=#262626 gui=NONE                                  cterm=NONE
hi StatusLineTermNC guifg=#666666 guibg=#151515 gui=NONE                                  cterm=NONE
hi VertSplit        guifg=#7d7d7d guibg=#262626 gui=NONE                                  cterm=NONE
hi Pmenu            guifg=#dedede guibg=#1c1c1c gui=NONE                                  cterm=NONE
hi PmenuSel         guifg=#ffffff guibg=#292929 gui=NONE                                  cterm=NONE
hi PmenuSbar        guifg=NONE    guibg=#1f1f1f gui=NONE                                  cterm=NONE
hi PmenuThumb       guifg=NONE    guibg=#212121 gui=NONE                                  cterm=NONE
hi TabLine          guifg=#ffffff guibg=#292929 gui=NONE                                  cterm=NONE
hi TabLineFill      guifg=NONE    guibg=#292929 gui=NONE                                  cterm=NONE
hi TabLineSel       guifg=#ffffff guibg=#292929 gui=NONE                                  cterm=NONE
hi NonText          guifg=#2b2b2b guibg=#121212 gui=NONE                                  cterm=NONE
hi SpecialKey       guifg=#95f59e guibg=NONE    gui=NONE                                  cterm=NONE
hi Folded           guifg=#787878 guibg=#303030 gui=NONE                                  cterm=NONE
hi Visual           guifg=NONE    guibg=#292929 gui=NONE                                  cterm=NONE
hi LineNr           guifg=#737373 guibg=#171717 gui=NONE                                  cterm=NONE
hi FoldColumn       guifg=#eeee00 guibg=#4d4d4d gui=NONE                                  cterm=NONE
hi CursorLine       guifg=NONE    guibg=#1c1c1c gui=NONE                                  cterm=NONE
hi CursorColumn     guifg=NONE    guibg=#1c1c1c gui=NONE                                  cterm=NONE
hi CursorLineNr     guifg=#969696 guibg=#1c1c1c gui=bold                                  cterm=bold
hi QuickFixLine     guifg=#333333 guibg=#f0e68c gui=NONE                                  cterm=NONE
hi SignColumn       guifg=NONE    guibg=NONE    gui=NONE    ctermfg=NONE    ctermbg=NONE  cterm=NONE
hi ErrorMsg         guifg=#ff0000 guibg=#ffffff gui=reverse                               cterm=reverse
hi ModeMsg          guifg=#d6b455 guibg=NONE    gui=bold                                  cterm=bold
hi WarningMsg       guifg=#cd5c5c guibg=NONE    gui=bold                                  cterm=bold
hi MoreMsg          guifg=#d66661 guibg=NONE    gui=bold                                  cterm=bold
hi Question         guifg=#d2ad3d guibg=NONE    gui=bold                                  cterm=bold
hi Todo             guifg=#ffffff guibg=NONE    gui=bold                                  cterm=bold
hi MatchParen       guifg=#ffffff guibg=#877a5b gui=NONE                                  cterm=NONE
hi Search           guifg=#f0e68c guibg=#42422c gui=NONE                                  cterm=NONE
hi IncSearch        guifg=#f0e68c guibg=#cd853f gui=NONE                                  cterm=NONE
hi WildMenu         guifg=#333333 guibg=#eeee00 gui=NONE                                  cterm=NONE
hi ColorColumn      guifg=#ffffff guibg=#cd5c5c gui=NONE                                  cterm=NONE
hi Cursor           guifg=#333333 guibg=#f0e68c gui=NONE                                  cterm=NONE
hi lCursor          guifg=#333333 guibg=#ff0000 gui=NONE                                  cterm=NONE
hi debugPC          guifg=#666666 guibg=NONE    gui=reverse cterm=reverse
hi debugBreakpoint  guifg=#ffa0a0 guibg=NONE    gui=reverse cterm=reverse
hi SpellBad         guifg=#cd5c5c guibg=NONE    guisp=#cd5c5c gui=undercurl cterm=underline
hi SpellCap         guifg=#75a0ff guibg=NONE    guisp=#75a0ff gui=undercurl cterm=underline
hi SpellLocal       guifg=#ffde9b guibg=NONE    guisp=#ffde9b gui=undercurl cterm=underline
hi SpellRare        guifg=#9acd32 guibg=NONE    guisp=#9acd32 gui=undercurl cterm=underline
hi Comment          guifg=#b8a281 guibg=NONE    gui=NONE cterm=NONE
hi Identifier       guifg=#77d6a6 guibg=NONE    gui=NONE cterm=NONE
hi Statement        guifg=#ff7659 guibg=NONE    gui=bold cterm=bold
hi Constant         guifg=#ffe57a guibg=NONE    gui=NONE cterm=NONE
hi PreProc          guifg=#cd5c5c guibg=NONE    gui=NONE cterm=NONE
hi Type             guifg=#93ba57 guibg=NONE    gui=bold cterm=bold
hi Special          guifg=#ffde9b guibg=NONE    gui=NONE cterm=NONE
hi Directory        guifg=#b8924f guibg=NONE    gui=NONE cterm=NONE
hi Conceal          guifg=#666666 guibg=NONE    gui=NONE cterm=NONE
hi Ignore           guifg=NONE    guibg=NONE    gui=NONE ctermfg=NONE ctermbg=NONE cterm=NONE
hi Title            guifg=#cd5c5c guibg=NONE    gui=bold cterm=bold
hi DiffAdd          guifg=#ffffff guibg=#5f875f gui=NONE cterm=NONE
hi DiffChange       guifg=#ffffff guibg=#5f87af gui=NONE cterm=NONE
hi DiffText         guifg=#000000 guibg=#c6c6c6 gui=NONE cterm=NONE
hi DiffDelete       guifg=#ffffff guibg=#af5faf gui=NONE cterm=NONE
